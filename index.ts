import { ApolloClient, InMemoryCache, gql } from '@apollo/client'
import { APIURL, allTokensQuery, tokensQuery } from './constants';
import { getDailyDataArray, getHourlyDataArray } from './utils';

// Function to fetch token price data
function fetchPrice<T extends keyof typeof APIURL>(tokenAddr: string, network: T, type: string) {
  // Create a new ApolloClient instance with specified URI and cache
  const client = new ApolloClient({
    uri: APIURL[network],
    cache: new InMemoryCache(),
  })

  // Make a GraphQL query to fetch token data
  client
    .query({
      query: gql(tokenAddr ? tokensQuery : allTokensQuery),
      variables: {
        token: tokenAddr ? tokenAddr.toLowerCase() : ""
      },
    })
    .then((resp) => {
      // Loop through the fetched tokens and display relevant information based on the day or month
      for (let j = 0; j < resp.data.tokens.length; j++) {
        const tokenData = resp.data.tokens[j];
        console.log("Token Name:", tokenData.name, "\tToken Symbol:", tokenData.symbol, "\tToken Address:", tokenData.id);
        switch (type) {
          case "1H":
            // Display hourly data for the token
            console.log(getHourlyDataArray(tokenData.tokenDayData));
            break;
          case "1D":
            // Display daily data for the token
            console.log(getDailyDataArray(tokenData.tokenDayData));
            break;
          default:
            // Display raw token data
            console.log(tokenData.tokenDayData);
            break;
        }
      }
    })
    .catch((err) => {
      // Handle errors that may occur during the data fetching process
      console.log('Error fetching data: ', err)
    })
}

// Extract command line arguments
const args = process.argv.slice(2);

// Check if the first argument is "eth" or "bsc" and fetch the token price data accordingly
if (args[0] == "eth" || args[0] == "bsc") {
  // Call the fetchPrice function with command line arguments
  fetchPrice(args[2], args[0], args[1]);
}
