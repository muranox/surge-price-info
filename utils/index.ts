/**
 * Represents the structure of data for a single token at a specific date.
 */
type TokenDataArray = {
    date: Date;
    price: number;
    dailyVolume: number;
    totalLiquidity: number;
};

/**
 * Represents the structure of raw token data received from thegraph.com.
 */
type TokenDayData = {
    id: string;
    date: number;
    priceUSD: string,
    dailyVolumeUSD: string,
    totalLiquidityUSD: string,
}

/**
 * Converts raw token data into a structured array for daily intervals.
 * @param tokenInfo - Raw token data for a specific hour.
 * @returns An array of TokenDataArray representing daily data.
 */
export function getDailyDataArray(tokenInfo: TokenDayData[]): TokenDataArray[] {
    return tokenInfo.map((entry) => {
        const convertedData = convertToTokenDataArray(entry);
        return {
            date: setDailyUpdateTimestamp(convertedData.date),
            price: convertedData.price,
            dailyVolume: convertedData.dailyVolume,
            totalLiquidity: convertedData.totalLiquidity,
        };
    });
}

/**
 * Converts raw token data into a structured array for hourly intervals within a day.
 * @param tokenInfo - Raw token data for a specific hour.
 * @returns An array of TokenDataArray representing hourly data.
 */
export function getHourlyDataArray(tokenInfo: TokenDayData[]): TokenDataArray[] {
    const hourlyDataArray: TokenDataArray[] = [];

    for (const entry of tokenInfo) {
        const convertedData = convertToTokenDataArray(entry);

        // Assuming each day has 24 hours
        for (let hour = 0; hour < 24; hour++) {
            const hourlyEntry = {
                date: new Date(convertedData.date.getTime() + hour * 60 * 60 * 1000),
                price: convertedData.price,
                dailyVolume: convertedData.dailyVolume,
                totalLiquidity: convertedData.totalLiquidity,
            };
            hourlyDataArray.push(hourlyEntry);
        }
    }

    return hourlyDataArray;
}

/**
 * Sets the timestamp for the next daily update, used for ensuring consistency in daily data representation.
 * @param date - Input date for conversion
 * @returns A Date object representing the next daily update timestamp.
 */
function setDailyUpdateTimestamp(date: Date): Date {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1, 0, 0, 0, 0);
}

/**
 * Converts raw token data into a structured TokenDataArray.
 * @param data - Raw token data for a specific hour.
 * @returns TokenDataArray representing the converted data.
 */
function convertToTokenDataArray(data: TokenDayData): TokenDataArray {
    return {
        date: new Date(data.date * 1000),
        price: parseFloat(data.priceUSD),
        dailyVolume: parseFloat(data.dailyVolumeUSD),
        totalLiquidity: parseFloat(data.totalLiquidityUSD),
    };
}
