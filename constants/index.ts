export const APIURL = {
  "bsc": 'https://api.thegraph.com/subgraphs/name/supermutecx/surge-info-bsc',
  "eth": 'https://api.thegraph.com/subgraphs/name/supermutecx/surge-info'
}

export const tokensQuery = `
    query Tokens($token: String) {
      tokens(where : {id : $token}) {
        id
        symbol
        name
        tokenDayData(orderBy: date, where: {totalLiquidityUSD_gt: "0"}) {
          id
          date
          priceUSD
          dailyVolumeUSD
          totalLiquidityUSD
        }
      }
    }
  `;

export const allTokensQuery = `
    query Tokens($token: String) {
      tokens {
        id
        symbol
        name
        tokenDayData(orderBy: date, where: {totalLiquidityUSD_gt: "0"}) {
          id
          date
          priceUSD
          dailyVolumeUSD
          totalLiquidityUSD
        }
      }
    }
  `;